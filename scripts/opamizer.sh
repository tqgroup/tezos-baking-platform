#! /usr/bin/env bash

set -e -o pipefail

root_default="$(dirname "$0")/.."
root=${tbp:-$root_default}

say () {
    echo "opamizer: $*" >&2
}

if ! [ -f "$root/scripts/opamizer.sh" ] ; then
    say "Wrong root path: $root (override with 'tbp' option)"
    exit 2
fi

usage () {
    cat >&2 <<EOF
Usage:

    [tbp=<PATH>] switch=NAME $0 {create,delete,enter,shell}

Create and use tezos-ish opam switches.

* tbp: Path to the tezos-baking-platform
       (default, inferred: '$root_default').
* switch: Name of the opam switch.
* nixps: Additional options to the \`enter\` nix-shell (e.g. \`-p emacs\`).

Examples:

     switch=tezosish-opam407 ./scripts/opamizer.sh create

Creates an opam switch that can build all the \`.opam\` files found in
the current directory.

     switch=tezosish-opam407 ./scripts/opamizer.sh shell

Enters a nix-shell + opam-environment.

EOF
}


nix_shell_run () {
    if [ "$just_script" = "true" ] ; then
        cat "$1"
    else
        nix-shell --pure "$root" -A tezos.master.opam-box --run "bash '$1'"
    fi
}

with_switch () {
    export switch_name="$switch"
    if [ "$switch_name" = "" ]; then
        say "Missing switch=... option"
        return 2
    fi
}

opam_root=$PWD/_opamizer_root

create_switch () {
    nix_script="$(mktemp -t build-opam-switch-XXXXX.bash)"
    with_switch
    cat > "$nix_script" <<EOF
set -e
export OPAMROOT=$opam_root
export OPAMYES=1
opam init --no-setup --bare --disable-sandboxing --disable-shell-hook --no-opamrc
opam update
opam switch create "$switch_name" ocaml-system.4.07.1
eval \$(opam env)
opams=\$(find . -name '*.opam' ! -size 0 -print)
opam install \$opams --deps-only --with-test
EOF
    nix_shell_run "$nix_script"
}

delete_switch () {
    with_switch
    nix_script="$(mktemp -t build-opam-switch-XXXXX.bash)"
    cat > "$nix_script" <<EOF
set -e
export OPAMROOT=$opam_root
opam switch remove "$switch_name"
EOF
    nix_shell_run "$nix_script"
}

enter_nix_shell () {
    with_switch
    nix_script="$(mktemp -t build-opam-switch-XXXXX.bash)"
    cat > "$nix_script" <<EOF
export OPAMROOT=$opam_root
export OPAMSWITCH="$switch_name"
eval \$(opam env --switch "$switch_name" --strict)
export PS1="[nix-shell|opam-$switch_name] $ "
opam switch
#bash -i
EOF
    nix-shell -p rlwrap -p jq $nixps --show-trace \
              --command ". '$nix_script' ; return" \
              '(tbp: (import tbp {}).tezos.master.opam-box.nativeBuildInputs) '"$root"
    # ACHTUNG: Proper whitespace is very important there  ———————————————————⤴
}

case "$1" in
    "create" ) create_switch ;;
    "delete" ) delete_switch ;;
    "enter" | "shell" ) enter_nix_shell ;;
    "help" ) usage ;;
    * )
        say "Unknown command: '$1'"
        usage ; exit 2 ;;
esac




