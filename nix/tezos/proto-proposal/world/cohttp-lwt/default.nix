/*opam-version: "2.0"
  name: "cohttp-lwt"
  version: "2.0.0"
  synopsis: "CoHTTP implementation using the Lwt concurrency
  library"
  description: """
  This is a portable implementation of HTTP that uses the Lwt
  concurrency library to multiplex IO.  It implements as much of the
  logic in an OS-independent way as possible, so that more specialised
  modules can be tailored for different targets.  For example, you
  can install `cohttp-lwt-unix` or `cohttp-lwt-jsoo` for a Unix or
  JavaScript backend, or `cohttp-mirage` for the MirageOS unikernel
  version of the library. All of these implementations share the same
  IO logic from this module."""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy"
    "Stefano Zacchiroli"
    "David Sheets"
    "Thomas Gazagnaire"
    "David Scott"
    "Rudi Grinberg"
    "Andy Ray"
  ]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-cohttp"
  doc: "https://mirage.github.io/ocaml-cohttp/"
  bug-reports: "https://github.com/mirage/ocaml-cohttp/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "dune" {build & >= "1.1.0"}
    "cohttp" {>= "2.0.0"}
    "lwt" {>= "2.5.0"}
    "sexplib0" {< "v0.13"}
    "ppx_sexp_conv" {>= "v0.9.0" & < "v0.13"}
    "logs"
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-cohttp.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-cohttp/releases/download/v2.0.0/cohttp-v2.0.0.tbz"
    checksum: [
      "md5=c354599fdb4f2625b6510182de0fc86b"
     
  "sha256=1f6914190100b86c713b67fb5d9c7121aa477600255deac05cd5a9a6530dc7cb"
     
  "sha512=6100392e9f399140864c99195741e5e46ce454d973a693bb2161f334592a55256253dd0912384480ce0da545e05bb24c13de894862b4e2081a48cf9231aa46ee"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, cohttp, lwt, sexplib0, ppx_sexp_conv, logs, findlib
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "2.0.0"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare dune "1.1.0") >= 0;
assert (vcompare cohttp "2.0.0") >= 0;
assert (vcompare lwt "2.5.0") >= 0;
assert (vcompare sexplib0 "v0.13") < 0;
assert (vcompare ppx_sexp_conv "v0.9.0") >= 0 && (vcompare ppx_sexp_conv
  "v0.13") < 0;

stdenv.mkDerivation rec {
  pname = "cohttp-lwt";
  version = "2.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-cohttp/releases/download/v2.0.0/cohttp-v2.0.0.tbz";
    sha256 = "1jy71m9sdafmbk0flp9501v4gai1f6f5vyv77dqnrf0004ci8s8z";
  };
  buildInputs = [
    ocaml dune cohttp lwt sexplib0 ppx_sexp_conv logs findlib ];
  propagatedBuildInputs = [
    ocaml dune cohttp lwt sexplib0 ppx_sexp_conv logs ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
