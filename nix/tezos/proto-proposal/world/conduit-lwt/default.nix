/*opam-version: "2.0"
  name: "conduit-lwt"
  version: "1.4.0"
  synopsis: "A portable network connection establishment library using
  Lwt"
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy" "Thomas Leonard" "Thomas Gazagnaire" "Rudi
  Grinberg"
  ]
  license: "ISC"
  tags: "org:mirage"
  homepage: "https://github.com/mirage/ocaml-conduit"
  bug-reports: "https://github.com/mirage/ocaml-conduit/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {build}
    "base-unix"
    "ppx_sexp_conv" {< "v0.13"}
    "sexplib" {< "v0.13"}
    "conduit" {= "1.4.0"}
    "lwt" {>= "3.0.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-conduit.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-conduit/releases/download/v1.4.0/conduit-v1.4.0.tbz"
    checksum: [
      "md5=204222b8a61692083b79c67c8967fb28"
     
  "sha256=8de1e44effbc252fbf5862ba874a5608b8b99552e978762008be2625faf51206"
     
  "sha512=44a9d10cdc286135ec5096bfd43c65bb3a80758291cc804211494616725069bfed1c3d4a79b1c07262b048e2909786e2cf39b0393f1388d835ab10ea5267fbf8"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, base-unix, ppx_sexp_conv, sexplib, conduit, lwt,
  findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.4.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare ppx_sexp_conv "v0.13") < 0;
assert (vcompare sexplib "v0.13") < 0;
assert stdenv.lib.getVersion conduit == "1.4.0";
assert (vcompare lwt "3.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "conduit-lwt";
  version = "1.4.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-conduit/releases/download/v1.4.0/conduit-v1.4.0.tbz";
    sha256 = "01hjypx2a9my10h7cy79aaavkf08ar58gfk2b2zjy9dwzx7f9qcd";
  };
  buildInputs = [
    ocaml dune base-unix ppx_sexp_conv sexplib conduit lwt findlib ];
  propagatedBuildInputs = [
    ocaml base-unix ppx_sexp_conv sexplib conduit lwt ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
