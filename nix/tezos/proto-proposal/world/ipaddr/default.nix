/*opam-version: "2.0"
  name: "ipaddr"
  version: "3.1.0"
  synopsis:
    "A library for manipulation of IP (and MAC) address
  representations"
  description: """
  Features:
   * Depends only on sexplib (conditionalization under consideration)
   * oUnit-based tests
   * IPv4 and IPv6 support
   * IPv4 and IPv6 CIDR prefix support
   * IPv4 and IPv6 [CIDR-scoped
  address](http://tools.ietf.org/html/rfc4291#section-2.3) support
   * `Ipaddr.V4` and `Ipaddr.V4.Prefix` modules are `Map.OrderedType`
   * `Ipaddr.V6` and `Ipaddr.V6.Prefix` modules are `Map.OrderedType`
   * `Ipaddr` and `Ipaddr.Prefix` modules are `Map.OrderedType`
   * `Ipaddr_unix` in findlib subpackage `ipaddr.unix` provides compatibility
  with the standard library `Unix` module
   * `Ipaddr_top` in findlib subpackage `ipaddr.top` provides top-level
  pretty printers (requires compiler-libs default since OCaml 4.0)
   * IP address scope classification
   * IPv4-mapped addresses in IPv6 (::ffff:0:0/96) are an embedding of IPv4
   * MAC-48 (Ethernet) address support
   * `Macaddr` is a `Map.OrderedType`
   * All types have sexplib serializers/deserializers"""
  maintainer: "anil@recoil.org"
  authors: ["David Sheets" "Anil Madhavapeddy" "Hugo Heuzard"]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-ipaddr"
  doc: "https://mirage.github.io/ocaml-ipaddr/"
  bug-reports: "https://github.com/mirage/ocaml-ipaddr/issues"
  depends: [
    "ocaml" {>= "4.04.0"}
    "dune" {build}
    "macaddr"
    "sexplib0"
    "ounit" {with-test}
    "ppx_sexp_conv" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-ipaddr.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-ipaddr/releases/download/v3.1.0/ipaddr-v3.1.0.tbz"
    checksum: [
      "md5=471a594563bb9c3dd91ae912b5ffd6ed"
     
  "sha256=39bfbbe2d650c26be7b5581d288eaedd1b47b842cdebaa08d6afd968983fc3b4"
     
  "sha512=7db4ee264e256944b26116f42d8f022e1278318260890f73d5a267cb1d9a5c2a0cf8b6da1c0fccf1469cd0c7ddaafde9da17e921f00d6d44a2a41f1aa64a1a64"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, macaddr, sexplib0, ounit ? null,
  ppx_sexp_conv ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "3.1.0"; in
assert (vcompare ocaml "4.04.0") >= 0;

stdenv.mkDerivation rec {
  pname = "ipaddr";
  version = "3.1.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-ipaddr/releases/download/v3.1.0/ipaddr-v3.1.0.tbz";
    sha256 = "1d637yc6indgsq4amsyd8aw4f6yxms72h7aqnpknphjhsvibpgrr";
  };
  buildInputs = [
    ocaml dune macaddr sexplib0 ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  stdenv.lib.optional
  doCheck
  ppx_sexp_conv
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml macaddr sexplib0 ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  stdenv.lib.optional
  doCheck
  ppx_sexp_conv;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
