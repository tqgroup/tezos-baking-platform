/*opam-version: "2.0"
  name: "ppx_fields_conv"
  version: "v0.12.0"
  synopsis: "Generation of accessor and iteration functions for ocaml
  records"
  description: "Part of the Jane Street's PPX rewriters
  collection."
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/ppx_fields_conv"
  doc:
   
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/ppx_fields_conv/index.html"
  bug-reports: "https://github.com/janestreet/ppx_fields_conv/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.12" & < "v0.13"}
    "fieldslib" {>= "v0.12" & < "v0.13"}
    "dune" {build & >= "1.5.1"}
    "ppxlib" {>= "0.5.0" & < "0.9.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/ppx_fields_conv.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.12/files/ppx_fields_conv-v0.12.0.tar.gz"
    checksum: [
      "md5=5bdf701197abc0dd4145a489912e49aa"
     
  "sha256=49be7fa34067c5c5d331433c48850d62c7feaeb5fba7490d965a0332b0f86962"
     
  "sha512=0574587461ab4334e3ab970224390544b9f022739b518562faedeb89751839667082adef3aaeac60f8530ae5670cb85a737ca4caec0080fbd945c22caa1dfdad"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, fieldslib, dune, ppxlib, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.12") >= 0 && (vcompare base "v0.13") < 0;
assert (vcompare fieldslib "v0.12") >= 0 && (vcompare fieldslib "v0.13") < 0;
assert (vcompare dune "1.5.1") >= 0;
assert (vcompare ppxlib "0.5.0") >= 0 && (vcompare ppxlib "0.9.0") < 0;

stdenv.mkDerivation rec {
  pname = "ppx_fields_conv";
  version = "v0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.12/files/ppx_fields_conv-v0.12.0.tar.gz";
    sha256 = "0qk9z2q340ssjq6lk9zvnnpgxiv21n2lhg23679wbib782ipzgj9";
  };
  buildInputs = [
    ocaml base fieldslib dune ppxlib findlib ];
  propagatedBuildInputs = [
    ocaml base fieldslib dune ppxlib ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
