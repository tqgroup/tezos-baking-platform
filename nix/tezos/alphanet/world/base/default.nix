/*opam-version: "2.0"
  name: "base"
  version: "v0.12.2"
  synopsis: "Full standard library replacement for OCaml"
  description: """
  Full standard library replacement for OCaml
  
  Base is a complete and portable alternative to the OCaml standard
  library. It provides all standard functionalities one would expect
  from a language standard library. It uses consistent conventions
  across all of its module.
  
  Base aims to be usable in any context. As a result system dependent
  features such as I/O are not offered by Base. They are instead
  provided by companion libraries such as stdio:
  
    https://github.com/janestreet/stdio"""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/base"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/base/index.html"
  bug-reports: "https://github.com/janestreet/base/issues"
  depends: [
    "ocaml" {>= "4.04.2" & < "4.09.0"}
    "sexplib0" {>= "v0.12" & < "v0.13"}
    "dune" {build & >= "1.5.1"}
  ]
  depopts: ["base-native-int63"]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/base.git"
  url {
    src: "https://github.com/janestreet/base/archive/v0.12.2.tar.gz"
    checksum: [
      "md5=7150e848a730369a2549d01645fb6c72"
     
  "sha256=0aa8fa8778412c67c38d40e9859bfa5871c4f9c25991f09fea201ae6aaf1d0d9"
     
  "sha512=d36a1cf6ffe91b7c51a53deedec529f27c235a01d99934e22cafa7554f91e1fe8e4599ca2c4483fe8910a80b8ee013877bd1e87ee65e0361648f22fb3a8b46c7"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, sexplib0, dune, findlib, base-native-int63 ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.2"; in
assert (vcompare ocaml "4.04.2") >= 0 && (vcompare ocaml "4.09.0") < 0;
assert (vcompare sexplib0 "v0.12") >= 0 && (vcompare sexplib0 "v0.13") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "base";
  version = "v0.12.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/janestreet/base/archive/v0.12.2.tar.gz";
    sha256 = "1nfhy6mfc6i0xagz14arqbww8waqzadqbsa0ip1nfb21g23zma0a";
  };
  buildInputs = [
    ocaml sexplib0 dune findlib ]
  ++
  stdenv.lib.optional
  (base-native-int63
  !=
  null)
  base-native-int63;
  propagatedBuildInputs = [
    ocaml sexplib0 dune ]
  ++
  stdenv.lib.optional
  (base-native-int63
  !=
  null)
  base-native-int63;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
