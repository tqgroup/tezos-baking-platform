/*opam-version: "2.0"
  name: "uucp"
  version: "12.0.0"
  synopsis: "Unicode character properties for OCaml"
  description: """
  Uucp is an OCaml library providing efficient access to a selection
  of
  character properties of the [Unicode character database][1].
  
  Uucp is independent from any Unicode text data structure and has
  no
  dependencies. It is distributed under the ISC license.
  
  [1]: http://www.unicode.org/reports/tr44/"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: [
    "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
    "David Kaloper Meršinjak <david@numm.org>"
  ]
  license: "ISC"
  tags: ["unicode" "text" "character" "org:erratique"]
  homepage: "https://erratique.ch/software/uucp"
  doc: "https://erratique.ch/software/uucp/doc/Uucp"
  bug-reports: "https://github.com/dbuenzli/uucp/issues"
  depends: [
    "ocaml" {>= "4.01.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build}
    "uchar"
    "uucd" {with-test}
    "uunf" {with-test}
    "uutf" {with-test}
  ]
  depopts: ["uunf" "uutf" "cmdliner"]
  conflicts: [
    "uutf" {< "1.0.1"}
    "cmdliner" {< "1.0.0"}
  ]
  build: [
    "ocaml"
    "pkg/pkg.ml"
    "build"
    "--dev-pkg"
    "%{pinned}%"
    "--with-uutf"
    "%{uutf:installed}%"
    "--with-uunf"
    "%{uunf:installed}%"
    "--with-cmdliner"
    "%{cmdliner:installed}%"
  ]
  dev-repo: "git+https://erratique.ch/repos/uucp.git"
  url {
    src: "https://erratique.ch/software/uucp/releases/uucp-12.0.0.tbz"
    checksum: [
      "md5=cf210ed43375b7f882c0540874e2cb81"
     
  "sha256=42eaf18e14fc99e29e2179700c1f191b64a597e6d50f176a8ca2007d89c4703f"
     
  "sha512=8cb682dc2a4f7da65a33b50271784b692e67db64e70fec1e2eef0ffdb261710a1c4035c4ec2303c94fb36204b8308b8970e67a8daa620ebd2b051868d413f604"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, uchar, uucd ? null,
  uunf ? null, uutf ? null, cmdliner ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "12.0.0"; in
assert (vcompare ocaml "4.01.0") >= 0;
assert uutf != null -> !((vcompare uutf "1.0.1") < 0);
assert cmdliner != null -> !((vcompare cmdliner "1.0.0") < 0);

stdenv.mkDerivation rec {
  pname = "uucp";
  version = "12.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://erratique.ch/software/uucp/releases/uucp-12.0.0.tbz";
    sha256 = "0gvhqj4ps052iim1f3ymwsbsar0v34ghqw3r46gf56gw2j7g3sj2";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg uchar ]
  ++
  stdenv.lib.optional
  doCheck
  uucd
  ++
  stdenv.lib.optional
  (doCheck
  ||
  uunf
  !=
  null)
  uunf
  ++
  stdenv.lib.optional
  (doCheck
  ||
  uutf
  !=
  null)
  uutf
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner;
  propagatedBuildInputs = [
    ocaml uchar ]
  ++
  stdenv.lib.optional
  doCheck
  uucd
  ++
  stdenv.lib.optional
  (doCheck
  ||
  uunf
  !=
  null)
  uunf
  ++
  stdenv.lib.optional
  (doCheck
  ||
  uutf
  !=
  null)
  uutf
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--dev-pkg'" "false"
      "'--with-uutf'"
      "${if uutf != null then "true" else "false"}" "'--with-uunf'" "${if
                                                                    uunf !=
                                                                    null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" "'--with-cmdliner'" "${if
                                                                    cmdliner
                                                                    != null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" ] ]; preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ") [ ]; installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done"; createFindlibDestdir = true;
      }
