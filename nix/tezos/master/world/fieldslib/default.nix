/*opam-version: "2.0"
  name: "fieldslib"
  version: "v0.13.0"
  synopsis:
    "Syntax extension to define first class values representing record
  fields, to get and set record fields, iterate and fold over all fields of a
  record and create new record values"
  description: """
  Part of Jane Street's Core library
  The Core suite of libraries is an industrial strength alternative to
  OCaml's standard library that was developed by Jane Street, the
  largest industrial user of OCaml."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/fieldslib"
  doc:
   
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/fieldslib/index.html"
  bug-reports: "https://github.com/janestreet/fieldslib/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.13" & < "v0.14"}
    "dune" {>= "1.5.1"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/fieldslib.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.13/files/fieldslib-v0.13.0.tar.gz"
    checksum: [
      "md5=3ac72dc49e43416c8b6fa0897bd4838b"
     
  "sha256=177887aa3d18b6e3af8409fe45757e42087ba6a8a5b9e9e8a991e6125758771d"
     
  "sha512=e7b55c55af2a178510d55c2e17a0cc4b18e1c7d6f0f30237925dbdfc36c8d9c3e408b83ce0a319061a8b7703e44188a762e5ef83ffc4cebf34be234592cdbaed"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.13.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.13") >= 0 && (vcompare base "v0.14") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "fieldslib";
  version = "v0.13.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.13/files/fieldslib-v0.13.0.tar.gz";
    sha256 = "07bpb1bi5rlim7lfkfd5m2k7n222grslbzh9hjpy7dhq7nm8fy0p";
  };
  buildInputs = [
    ocaml base dune findlib ];
  propagatedBuildInputs = [
    ocaml base dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
