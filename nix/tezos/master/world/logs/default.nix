/*opam-version: "2.0"
  name: "logs"
  version: "0.7.0"
  synopsis: "Logging infrastructure for OCaml"
  description: """
  Logs provides a logging infrastructure for OCaml. Logging is performed
  on sources whose reporting level can be set independently. Log
  message
  report is decoupled from logging and is handled by a reporter.
  
  A few optional log reporters are distributed with the base library and
  the API easily allows to implement your own.
  
  `Logs` has no dependencies. The optional `Logs_fmt` reporter on
  OCaml
  formatters depends on [Fmt][fmt].  The optional `Logs_browser`
  reporter that reports to the web browser console depends
  on
  [js_of_ocaml][jsoo]. The optional `Logs_cli` library that provides
  command line support for controlling Logs depends on
  [`Cmdliner`][cmdliner]. The optional `Logs_lwt` library that provides
  Lwt logging functions depends on [`Lwt`][lwt]
  
  Logs and its reporters are distributed under the ISC license.
  
  [fmt]: http://erratique.ch/software/fmt
  [jsoo]: http://ocsigen.org/js_of_ocaml/
  [cmdliner]: http://erratique.ch/software/cmdliner
  [lwt]: http://ocsigen.org/lwt/"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "The logs programmers"
  license: "ISC"
  tags: ["log" "system" "org:erratique"]
  homepage: "https://erratique.ch/software/logs"
  doc: "https://erratique.ch/software/logs/doc"
  bug-reports: "https://github.com/dbuenzli/logs/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build}
    "mtime" {with-test}
  ]
  depopts: ["js_of_ocaml" "fmt" "cmdliner" "lwt" "base-threads"]
  conflicts: [
    "js_of_ocaml" {< "3.3.0"}
  ]
  build: [
    "ocaml"
    "pkg/pkg.ml"
    "build"
    "--pinned"
    "%{pinned}%"
    "--with-js_of_ocaml"
    "%{js_of_ocaml:installed}%"
    "--with-fmt"
    "%{fmt:installed}%"
    "--with-cmdliner"
    "%{cmdliner:installed}%"
    "--with-lwt"
    "%{lwt:installed}%"
    "--with-base-threads"
    "%{base-threads:installed}%"
  ]
  dev-repo: "git+https://erratique.ch/repos/logs.git"
  url {
    src: "https://erratique.ch/software/logs/releases/logs-0.7.0.tbz"
    checksum: [
      "md5=2bf021ca13331775e33cf34ab60246f7"
     
  "sha256=86f4a02807eb1a297aae44977d9f61e419c31458a5d7b23c6f55575e8e69d5ca"
     
  "sha512=657f66eeaf5788947a3ff4b2a179f7e192ad4ce086fc8968286066d4b83af476523b3befce50ee58abf2d9b8c8dc65573ba8ed62f9c5e7933db29a7fb769646a"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, mtime ? null,
  js_of_ocaml ? null, fmt ? null, cmdliner ? null, lwt ? null,
  base-threads ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.7.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert js_of_ocaml != null -> !((vcompare js_of_ocaml "3.3.0") < 0);

stdenv.mkDerivation rec {
  pname = "logs";
  version = "0.7.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://erratique.ch/software/logs/releases/logs-0.7.0.tbz";
    sha256 = "1jnmd675wmsmdwyb5mx5b0ac66g4c6gpv5s4mrx2j6pb0wla1x46";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg ]
  ++
  stdenv.lib.optional
  doCheck
  mtime
  ++
  stdenv.lib.optional
  (js_of_ocaml
  !=
  null)
  js_of_ocaml
  ++
  stdenv.lib.optional
  (fmt
  !=
  null)
  fmt
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner
  ++
  stdenv.lib.optional
  (lwt
  !=
  null)
  lwt
  ++
  stdenv.lib.optional
  (base-threads
  !=
  null)
  base-threads;
  propagatedBuildInputs = [
    ocaml ]
  ++
  stdenv.lib.optional
  doCheck
  mtime
  ++
  stdenv.lib.optional
  (js_of_ocaml
  !=
  null)
  js_of_ocaml
  ++
  stdenv.lib.optional
  (fmt
  !=
  null)
  fmt
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner
  ++
  stdenv.lib.optional
  (lwt
  !=
  null)
  lwt
  ++
  stdenv.lib.optional
  (base-threads
  !=
  null)
  base-threads;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pinned'" "false"
      "'--with-js_of_ocaml'"
      "${if js_of_ocaml != null then "true" else "false"}" "'--with-fmt'" "${if
                                                                    fmt !=
                                                                    null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" "'--with-cmdliner'" "${if
                                                                    cmdliner
                                                                    != null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" "'--with-lwt'" "${if
                                                                    lwt !=
                                                                    null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" "'--with-base-threads'" "${if
                                                                    base-threads
                                                                    != null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" ] ]; preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ") [ ]; installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done"; createFindlibDestdir = true; }
