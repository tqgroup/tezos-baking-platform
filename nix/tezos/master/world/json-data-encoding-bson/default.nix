/*opam-version: "2.0"
  name: "json-data-encoding-bson"
  version: "0.8"
  synopsis: "Type-safe encoding to and decoding from JSON (bson
  support)"
  maintainer: "contact@nomadic-labs.com"
  authors: ["Nomadic Labs" "Ocamlpro"]
  license: "LGPLv3 w/ linking exception"
  homepage: "https://gitlab.com/nomadic-labs/json-data-encoding"
  bug-reports:
  "https://gitlab.com/nomadic-labs/json-data-encoding/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {>= "1.7"}
    "json-data-encoding" {= version}
    "ocplib-endian" {>= "1.0"}
  ]
  build: ["dune" "build" "-j" jobs "-p" name]
  dev-repo: "git+https://gitlab.com/nomadic-labs/json-data-encoding"
  url {
    src:
     
  "https://gitlab.com/nomadic-labs/json-data-encoding/-/archive/v0.8/json-data-encoding-v0.8.tar.gz"
    checksum: [
      "md5=24fcb7feb10395eaaf840d1f8ccf162f"
     
  "sha512=f6d2a00008a0cd88bfae2458f56dde8f45c4e209ece9805d3e8d2c74401e4a5f9e12f99d5318f58a30a4579d9c9e9f204a75269c2110bb16a3e1036b2599b7a5"
     
  "sha256=3f940293fec23e5bc3d0ade6d30d6d8b39adff5bc6f2cd3821d0189b2b5c7fd7"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, json-data-encoding, ocplib-endian, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare dune "1.7") >= 0;
assert stdenv.lib.getVersion json-data-encoding == version;
assert (vcompare ocplib-endian "1.0") >= 0;

stdenv.mkDerivation rec {
  pname = "json-data-encoding-bson";
  version = "0.8";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://gitlab.com/nomadic-labs/json-data-encoding/-/archive/v0.8/json-data-encoding-v0.8.tar.gz";
    sha256 = "1mvzbhmrn66h44wcvwn6bgzssfcbdl6x7rmds31mngn2zs9h551z";
  };
  buildInputs = [
    ocaml dune json-data-encoding ocplib-endian findlib ];
  propagatedBuildInputs = [
    ocaml dune json-data-encoding ocplib-endian ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-j'" "1" "'-p'" pname ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
