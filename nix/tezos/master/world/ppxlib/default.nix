/*opam-version: "2.0"
  name: "ppxlib"
  version: "0.12.0"
  synopsis: "Base library and tools for ppx rewriters"
  description: """
  A comprehensive toolbox for ppx development. It features:
  - a OCaml AST / parser / pretty-printer snapshot,to create a full
     frontend independent of the version of OCaml;
  - a library for library for ppx rewriters in general, and type-driven
    code generators in particular;
  - a feature-full driver for OCaml AST transformers;
  - a quotation mechanism allowing  to write values representing the
     OCaml AST in the OCaml syntax;
  - a generator of open recursion classes from type
  definitions."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/ocaml-ppx/ppxlib"
  doc: "https://ocaml-ppx.github.io/ppxlib/"
  bug-reports: "https://github.com/ocaml-ppx/ppxlib/issues"
  depends: [
    "ocaml" {>= "4.04.1" & < "4.11.0"}
    "base" {>= "v0.11.0"}
    "dune" {>= "1.11"}
    "ocaml-compiler-libs" {>= "v0.11.0"}
    "ocaml-migrate-parsetree" {>= "1.3.1"}
    "ppx_derivers" {>= "1.0"}
    "stdio" {>= "v0.11.0"}
    "ocamlfind" {with-test}
    "cinaps" {with-test & >= "v0.12.1"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  run-test:
    ["dune" "runtest" "-p" name "-j" jobs]
      {ocaml:version >= "4.06" & ocaml:version < "4.08"}
  dev-repo: "git+https://github.com/ocaml-ppx/ppxlib.git"
  url {
    src: "https://github.com/ocaml-ppx/ppxlib/archive/0.12.0.tar.gz"
    checksum: [
     
  "sha256=6b562c9b3b9350777318729921f890850b385c469db60769aafd9371998a2c42"
     
  "sha512=2372a7a53d857389978e617c95183289547d53caa5e83a7d867cab347b114b719667bd09eaf2e2334085ef92691a99b42871f6410ffb2977b0b8724014c80a70"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, dune, ocaml-compiler-libs, ocaml-migrate-parsetree,
  ppx_derivers, stdio, findlib, cinaps ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.12.0"; in
assert (vcompare ocaml "4.04.1") >= 0 && (vcompare ocaml "4.11.0") < 0;
assert (vcompare base "v0.11.0") >= 0;
assert (vcompare dune "1.11") >= 0;
assert (vcompare ocaml-compiler-libs "v0.11.0") >= 0;
assert (vcompare ocaml-migrate-parsetree "1.3.1") >= 0;
assert (vcompare ppx_derivers "1.0") >= 0;
assert (vcompare stdio "v0.11.0") >= 0;
assert doCheck -> (vcompare cinaps "v0.12.1") >= 0;

stdenv.mkDerivation rec {
  pname = "ppxlib";
  version = "0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ppxlib/archive/0.12.0.tar.gz";
    sha256 = "0hiciacp34zxm9lhgdlx8rf3h2w5j3w236bj31rpfl4k7fdjqmkb";
  };
  buildInputs = [
    ocaml base dune ocaml-compiler-libs ocaml-migrate-parsetree ppx_derivers
    stdio findlib cinaps ];
  propagatedBuildInputs = [
    ocaml base dune ocaml-compiler-libs ocaml-migrate-parsetree ppx_derivers
    stdio ]
  ++
  stdenv.lib.optional
  doCheck
  findlib
  ++
  [
    cinaps ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
