/*opam-version: "2.0"
  name: "num"
  version: "1.3"
  synopsis:
    "The legacy Num library for arbitrary-precision integer and rational
  arithmetic"
  maintainer: "Xavier Leroy <xavier.leroy@inria.fr>"
  authors: ["Valérie Ménissier-Morain" "Pierre Weis" "Xavier
  Leroy"]
  license: "LGPL-2.1-only with OCaml-LGPL-linking-exception"
  homepage: "https://github.com/ocaml/num/"
  bug-reports: "https://github.com/ocaml/num/issues"
  depends: [
    "ocaml" {>= "4.06.0"}
    "ocamlfind" {build & >= "1.7.3"}
  ]
  conflicts: ["base-num"]
  build: make
  install: [
    make
    "install" {!ocaml:preinstalled}
    "findlib-install" {ocaml:preinstalled}
  ]
  patches: "installation-warning.patch"
  dev-repo: "git+https://github.com/ocaml/num.git"
  extra-files: [
    "installation-warning.patch" "md5=93c92bf6da6bae09d068da42b1bbaaac"
  ]
  url {
    src: "https://github.com/ocaml/num/archive/v1.3.tar.gz"
    checksum: [
      "md5=f074e12325e84ebc883b37e5db10403d"
     
  "sha256=4f79c30e81ea9553c5b2c5b5b57bb19968ccad1e85256b3c446b5df58f33e94d"
     
  "sha512=c88310f8c45700990095e6b2e9abf24c27347711b9abfd1dde75e540fbbfc6a365e6713bd69f66009af305728fcb36dc61eb37fdd0be7d73824b0e92fbe8c031"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.3"; in
assert (vcompare ocaml "4.06.0") >= 0;
assert (vcompare findlib "1.7.3") >= 0;

stdenv.mkDerivation rec {
  pname = "num";
  version = "1.3";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/num/archive/v1.3.tar.gz";
    sha256 = "0kg96f7zapbb8hy6n9c53snwqs4rn5xvbdf5nb2m75gah47c6yag";
  };
  postUnpack = "ln -sv ${./installation-warning.patch} \"$sourceRoot\"/installation-warning.patch";
  buildInputs = [
    ocaml findlib ];
  propagatedBuildInputs = [
    ocaml findlib ];
  configurePhase = "true";
  patches = [
    "installation-warning.patch" ];
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'findlib-install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
