/*opam-version: "2.0"
  name: "topkg"
  version: "1.0.1"
  synopsis: "The transitory OCaml software packager"
  description: """
  Topkg is a packager for distributing OCaml software. It provides an
  API to describe the files a package installs in a given build
  configuration and to specify information about the package's
  distribution, creation and publication procedures.
  
  The optional topkg-care package provides the `topkg` command line tool
  which helps with various aspects of a package's life cycle: creating
  and linting a distribution, releasing it on the WWW, publish
  its
  documentation, add it to the OCaml opam repository, etc.
  
  Topkg is distributed under the ISC license and has **no**
  dependencies. This is what your packages will need as a
  *build*
  dependency.
  
  Topkg-care is distributed under the ISC license it depends on
  [fmt][fmt], [logs][logs], [bos][bos],
  [cmdliner][cmdliner],
  [webbrowser][webbrowser] and `opam-format`.
  
  [fmt]: http://erratique.ch/software/fmt
  [logs]: http://erratique.ch/software/logs
  [bos]: http://erratique.ch/software/bos
  [cmdliner]: http://erratique.ch/software/cmdliner
  [webbrowser]: http://erratique.ch/software/webbrowser"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  license: "ISC"
  tags: ["packaging" "ocamlbuild" "org:erratique"]
  homepage: "http://erratique.ch/software/topkg"
  doc: "http://erratique.ch/software/topkg/doc"
  bug-reports: "https://github.com/dbuenzli/topkg/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "ocamlfind" {build & >= "1.6.1"}
    "ocamlbuild"
  ]
  build: [
    "ocaml" "pkg/pkg.ml" "build" "--pkg-name" name "--dev-pkg"
  "%{pinned}%"
  ]
  dev-repo: "git+http://erratique.ch/repos/topkg.git"
  url {
    src: "http://erratique.ch/software/topkg/releases/topkg-1.0.1.tbz"
    checksum: [
      "md5=16b90e066d8972a5ef59655e7c28b3e9"
     
  "sha256=0044aea6ad245703836a7faa792a36748736ab9f06e971f1dfb016635a6edaa3"
     
  "sha512=0a19f7727553659f2bb87ace3c52591cce07e1ebc5bf1f1d4feb2d339fc4cf6367f3b7c5d8eccf9b6982d6681b928b27f2b55607cd9a592cb8d4006a46ee92cb"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.0.1"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare findlib "1.6.1") >= 0;

stdenv.mkDerivation rec {
  pname = "topkg";
  version = "1.0.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "http://erratique.ch/software/topkg/releases/topkg-1.0.1.tbz";
    sha256 = "18ysdrd665mhvzqp3s86kymkd1vl6qm7kakzda1h6mr4mnkawi00";
  };
  buildInputs = [
    ocaml findlib ocamlbuild ];
  propagatedBuildInputs = [
    ocaml findlib ocamlbuild ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pkg-name'" pname "'--dev-pkg'"
      "false" ]
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
