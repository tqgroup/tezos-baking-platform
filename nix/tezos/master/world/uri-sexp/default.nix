/*opam-version: "2.0"
  name: "uri-sexp"
  version: "3.1.0"
  synopsis: "An RFC3986 URI/URL parsing library"
  description: "ocaml-uri with sexp support"
  maintainer: "anil@recoil.org"
  authors: ["Anil Madhavapeddy" "David Sheets" "Rudi Grinberg"]
  license: "ISC"
  tags: ["url" "uri" "org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-uri"
  doc: "https://mirage.github.io/ocaml-uri/"
  bug-reports: "https://github.com/mirage/ocaml-uri/issues"
  depends: [
    "ocaml"
    "uri" {= version}
    "dune" {>= "1.2.0"}
    "ppx_sexp_conv" {>= "v0.9.0"}
    "sexplib0"
    "ounit" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-uri.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-uri/releases/download/v3.1.0/uri-v3.1.0.tbz"
    checksum: [
     
  "sha256=c452823fd870cf7cffe51aef3e9ca646a382dc6f87282f2b16bfe30a7515ac43"
     
  "sha512=c015576bb077fd243022bcd8804e628d23a253dcd8bbdda8dc2a57e86cfeb9fd629087ec7d7e23dc71dd7cd137450ca2c5ecf8fb7d184ec9d1d4e41f6f83ee38"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, uri, dune, ppx_sexp_conv, sexplib0, ounit ? null, findlib
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "3.1.0"; in
assert stdenv.lib.getVersion uri == version;
assert (vcompare dune "1.2.0") >= 0;
assert (vcompare ppx_sexp_conv "v0.9.0") >= 0;

stdenv.mkDerivation rec {
  pname = "uri-sexp";
  version = "3.1.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-uri/releases/download/v3.1.0/uri-v3.1.0.tbz";
    sha256 = "0hxc2mshmqxz2qmjya47dzf858s6lsf3xvqswpzprkvhv0zq4ln4";
  };
  buildInputs = [
    ocaml uri dune ppx_sexp_conv sexplib0 ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml uri dune ppx_sexp_conv sexplib0 ]
  ++
  stdenv.lib.optional
  doCheck
  ounit;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
