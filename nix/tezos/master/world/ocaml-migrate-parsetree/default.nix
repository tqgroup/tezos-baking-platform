/*opam-version: "2.0"
  name: "ocaml-migrate-parsetree"
  version: "1.5.0"
  synopsis: "Convert OCaml parsetrees between different versions"
  description: """
  Convert OCaml parsetrees between different versions
  
  This library converts parsetrees, outcometree and ast mappers
  between
  different OCaml versions.  High-level functions help making PPX
  rewriters independent of a compiler version."""
  maintainer: "frederic.bour@lakaban.net"
  authors: [
    "Frédéric Bour <frederic.bour@lakaban.net>"
    "Jérémie Dimino <jeremie@dimino.org>"
  ]
  license: "LGPL-2.1 with OCaml linking exception"
  tags: ["syntax" "org:ocamllabs"]
  homepage: "https://github.com/ocaml-ppx/ocaml-migrate-parsetree"
  doc: "https://ocaml-ppx.github.io/ocaml-migrate-parsetree/"
  bug-reports:
  "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/issues"
  depends: [
    "result"
    "ppx_derivers"
    "dune" {>= "1.9.0"}
    "ocaml" {>= "4.02.3"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocaml-ppx/ocaml-migrate-parsetree.git"
  url {
    src:
     
  "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/releases/download/v1.5.0/ocaml-migrate-parsetree-v1.5.0.tbz"
    checksum: [
     
  "sha256=7f56679c9561552762666de5b6b81c8e4cc2e9fd92272e2269878a2eb534e3c0"
     
  "sha512=87fdccafae83b0437f1ccd4f3cfbc49e699bc0804596480e0df88510ba33410f31d48c7f677fe72800ed3f442a3a586d82d86aee1d12a964f79892833847b16a"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml-result, ppx_derivers, dune, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.5.0"; in
assert (vcompare dune "1.9.0") >= 0;
assert (vcompare ocaml "4.02.3") >= 0;

stdenv.mkDerivation rec {
  pname = "ocaml-migrate-parsetree";
  version = "1.5.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/releases/download/v1.5.0/ocaml-migrate-parsetree-v1.5.0.tbz";
    sha256 = "1h736jsjx2l7d4i2w9wjzplw4k4f3jwbdrbdcri2fmb1jnf6fmkz";
  };
  buildInputs = [
    ocaml-result ppx_derivers dune ocaml findlib ];
  propagatedBuildInputs = [
    ocaml-result ppx_derivers dune ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
