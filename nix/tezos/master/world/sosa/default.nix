/*opam-version: "2.0"
  name: "sosa"
  version: "0.3.0"
  synopsis: "Sane OCaml String API"
  description: """
  The Sosa library is a set of APIs (module types) that define what a
  string of characters should be, and a set of modules and functors
  that
  implement them."""
  maintainer: "seb@mondet.org"
  authors: [
    "Sebastien Mondet <seb@mondet.org>"
    "Leonid Rozenberg <leonidr@gmail.com>"
    "Isaac Hodes <isaachodes@gmail.com>"
    "Jeff Hammerbacher <jeff.hammerbacher@gmail.com>"
  ]
  homepage:
  "http://www.hammerlab.org/docs/sosa/master/index.html"
  bug-reports: "https://github.com/hammerlab/sosa/issues"
  depends: [
    "ocaml" {>= "4.02.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
  ]
  install: [
    [make "build"]
    [make "install"]
  ]
  remove: [make "uninstall"]
  dev-repo: "git+https://github.com/hammerlab/sosa.git"
  url {
    src: "https://github.com/hammerlab/sosa/archive/sosa.0.3.0.tar.gz"
    checksum: [
      "md5=5d21343b5960f014c182b20f028eb27f"
     
  "sha256=576883f9c5217be21dd7ef6fb1edc4fc282d84aed5deb9935c3353ccf939a351"
     
  "sha512=09cd52812725ba2651a8f9b518c0229f934483ca84141e893d3b311992912b3d4bd1dd04e5c04ade0dd7520fff9fbb33516f2f65aee0d6081015314db7fa8d59"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.3.0"; in
assert (vcompare ocaml "4.02.0") >= 0;

stdenv.mkDerivation rec {
  pname = "sosa";
  version = "0.3.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/hammerlab/sosa/archive/sosa.0.3.0.tar.gz";
    sha256 = "0ld377wwqlrkbj9vkpnmms22sa7wqknv2vzgswfy4yr1qpwq6s2p";
  };
  buildInputs = [
    ocaml findlib ocamlbuild ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'build'" ] [ "make" "'install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
