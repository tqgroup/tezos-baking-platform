/*opam-version: "2.0"
  name: "js_of_ocaml-compiler"
  version: "3.5.0"
  synopsis: "Compiler from OCaml bytecode to Javascript"
  description: """
  Js_of_ocaml is a compiler from OCaml bytecode to JavaScript.
  It makes it possible to run pure OCaml programs in JavaScript
  environment like browsers and Node.js"""
  maintainer: "dev@ocsigen.org"
  authors: "Ocsigen team"
  homepage: "http://ocsigen.github.io/js_of_ocaml"
  bug-reports: "https://github.com/ocsigen/js_of_ocaml/issues"
  depends: [
    "ocaml" {>= "4.02.0"}
    "dune" {>= "1.11.1"}
    "ppx_expect" {with-test & >= "0.12.0"}
    "cmdliner"
    "ocaml-migrate-parsetree"
    "yojson"
  ]
  depopts: ["ocamlfind"]
  conflicts: [
    "ocamlfind" {< "1.5.1"}
    "js_of_ocaml" {< "3.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocsigen/js_of_ocaml.git"
  url {
    src:
     
  "https://github.com/ocsigen/js_of_ocaml/releases/download/3.5.0/js_of_ocaml-3.5.0.tbz"
    checksum: [
     
  "sha256=c19903c11a7e7657a4bb92de9ad910e6daf5afc9da7222dcd8fe8da8867f05b7"
     
  "sha512=3aa852552abe268504f7e26841bd1c43e95dba0a5827f70b97230e900909c3fa94f59715b45ce658f0c2e298a0e89c882a994d5fcc6bcf75250cf0b94733fa88"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ppx_expect ? null, cmdliner,
  ocaml-migrate-parsetree, yojson, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "3.5.0"; in
assert (vcompare ocaml "4.02.0") >= 0;
assert (vcompare dune "1.11.1") >= 0;
assert doCheck -> (vcompare ppx_expect "0.12.0") >= 0;
assert !((vcompare findlib "1.5.1") < 0);

stdenv.mkDerivation rec {
  pname = "js_of_ocaml-compiler";
  version = "3.5.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocsigen/js_of_ocaml/releases/download/3.5.0/js_of_ocaml-3.5.0.tbz";
    sha256 = "1dq5gy3ai3gyv3f24wnsr6pzbnp623crmpljpfj5fxky3b0h76f1";
  };
  buildInputs = [
    ocaml dune ppx_expect cmdliner ocaml-migrate-parsetree yojson findlib ];
  propagatedBuildInputs = [
    ocaml dune ppx_expect cmdliner ocaml-migrate-parsetree yojson ]
  ++
  stdenv.lib.optional
  (findlib
  !=
  null)
  findlib;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
