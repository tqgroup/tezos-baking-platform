/*opam-version: "2.0"
  name: "magic-mime"
  version: "1.1.2"
  synopsis: "Map filenames to common MIME types"
  description: """
  This library contains a database of MIME types that maps filename
  extensions
  into MIME types suitable for use in many Internet protocols such as HTTP
  or
  e-mail.  It is generated from the `mime.types` file found in Unix systems,
  but
  has no dependency on a filesystem since it includes the contents of
  the
  database as an ML datastructure.
  
  For example, here's how to lookup MIME types in the [utop] REPL:
  
      #require "magic-mime";;
      Magic_mime.lookup "/foo/bar.txt";;
      - : bytes = "text/plain"
      Magic_mime.lookup "bar.css";;
      - : bytes = "text/css\""""
  maintainer: "Anil Madhavapeddy <anil@recoil.org>"
  authors: ["Anil Madhavapeddy" "Maxence Guesdon"]
  license: "ISC"
  homepage: "https://github.com/mirage/ocaml-magic-mime"
  doc: "https://mirage.github.io/ocaml-magic-mime/"
  bug-reports: "https://github.com/mirage/ocaml-magic-mime/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune"
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-magic-mime.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-magic-mime/releases/download/v1.1.2/magic-mime-v1.1.2.tbz"
    checksum: [
     
  "sha256=0c590bbc747531b56d392ee8f063d879df1e2026ba2dfa2d1bc98c9a9acb04eb"
     
  "sha512=8264db78adc2c75b8adabc23c26ad34eab98383bd3a8f2068f2236ff3425d653c0238fbd7361e55a3d70d843413ef8671b6e97293074b4d3a1e300628d5292ab"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.1.2"; in
assert (vcompare ocaml "4.03.0") >= 0;

stdenv.mkDerivation rec {
  pname = "magic-mime";
  version = "1.1.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-magic-mime/releases/download/v1.1.2/magic-mime-v1.1.2.tbz";
    sha256 = "1sq4rfd9m3693cnzlbds4qh1xpvrv1iz1s1f75nvacbmfjy0nn8c";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
