/*opam-version: "2.0"
  name: "ptime"
  version: "0.8.5"
  synopsis: "POSIX time for OCaml"
  description: """
  Ptime has platform independent POSIX time support in pure OCaml. It
  provides a type to represent a well-defined range of POSIX timestamps
  with picosecond precision, conversion with date-time values,
  conversion with [RFC 3339 timestamps][rfc3339] and pretty printing to
  a
  human-readable, locale-independent representation.
  
  The additional Ptime_clock library provides access to a system POSIX
  clock and to the system's current time zone offset.
  
  Ptime is not a calendar library.
  
  Ptime depends on the `result` compatibility package. Ptime_clock
  depends on your system library. Ptime_clock's optional JavaScript
  support depends on [js_of_ocaml][jsoo]. Ptime and its libraries
  are
  distributed under the ISC license.
  
  [rfc3339]: http://tools.ietf.org/html/rfc3339
  [jsoo]: http://ocsigen.org/js_of_ocaml/"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "The ptime programmers"
  license: "ISC"
  tags: ["time" "posix" "system" "org:erratique"]
  homepage: "https://erratique.ch/software/ptime"
  doc: "https://erratique.ch/software/ptime/doc"
  bug-reports: "https://github.com/dbuenzli/ptime/issues"
  depends: [
    "ocaml" {>= "4.01.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build}
    "result"
  ]
  depopts: ["js_of_ocaml"]
  conflicts: [
    "js_of_ocaml" {< "3.3.0"}
  ]
  build: [
    "ocaml"
    "pkg/pkg.ml"
    "build"
    "--pinned"
    "%{pinned}%"
    "--with-js_of_ocaml"
    "%{js_of_ocaml:installed}%"
  ]
  dev-repo: "git+http://erratique.ch/repos/ptime.git"
  url {
    src: "https://erratique.ch/software/ptime/releases/ptime-0.8.5.tbz"
    checksum: [
      "md5=4d48055d623ecf2db792439b3e96a520"
     
  "sha256=47c82848d26da735495c2fb9c2ed4fc45971d5a9ee97bd6d735faae0fb29b8bb"
     
  "sha512=fb1434834ce4b3c20347d9e98a3a36d687165446773992bdb58428fec05adeecc21913f383f679feafa420bd62928728054aac24d96413db61f0a83f1c2e66ea"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, ocaml-result,
  js_of_ocaml ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8.5"; in
assert (vcompare ocaml "4.01.0") >= 0;
assert js_of_ocaml != null -> !((vcompare js_of_ocaml "3.3.0") < 0);

stdenv.mkDerivation rec {
  pname = "ptime";
  version = "0.8.5";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://erratique.ch/software/ptime/releases/ptime-0.8.5.tbz";
    sha256 = "1fxq57xy1ajzfdnvv5zfm7ap2nf49znw5f9gbi4kb9vds942ij27";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg ocaml-result ]
  ++
  stdenv.lib.optional
  (js_of_ocaml
  !=
  null)
  js_of_ocaml;
  propagatedBuildInputs = [
    ocaml ocaml-result ]
  ++
  stdenv.lib.optional
  (js_of_ocaml
  !=
  null)
  js_of_ocaml;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pinned'" "false"
      "'--with-js_of_ocaml'"
      "${if js_of_ocaml != null then "true" else "false"}" ] ];
    preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
    [ ];
    installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
    createFindlibDestdir = true;
  }
