/*opam-version: "2.0"
  name: "dune-configurator"
  version: "1.0.0"
  synopsis: ""
  description: "dune.configurator library distributed with Dune
  1.x"
  maintainer: "Jérémie Dimino"
  authors: "Jérémie Dimino"
  homepage: "https://github.com/ocaml/dune"
  bug-reports: "https://github.com/ocaml/dune/issues"
  depends: [
    "ocaml"
    "dune" {< "2.0.0"}
  ]*/
{ runCommand, doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv,
  opam, fetchurl, ocaml, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.0.0"; in
assert (vcompare dune "2.0.0") < 0;

stdenv.mkDerivation rec {
  pname = "dune-configurator";
  version = "1.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = runCommand
  "empty"
  {
    outputHashMode = "recursive";
    outputHashAlgo = "sha256";
    outputHash = "0sjjj9z1dhilhpc8pq4154czrb79z9cm044jvn75kxcjv6v5l2m5";
  }
  "mkdir $out";
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
