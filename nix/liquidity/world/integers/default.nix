/*opam-version: "2.0"
  name: "integers"
  version: "0.2.2"
  synopsis: "Various signed and unsigned integer types for OCaml"
  maintainer: "yallop@gmail.com"
  authors: [
    "Jeremy Yallop" "Demi Obenour" "Stephane Glondu" "Andreas
  Hauptmann"
  ]
  license: "MIT"
  homepage: "https://github.com/ocamllabs/ocaml-integers"
  doc: "http://ocamllabs.github.io/ocaml-integers/api.docdir/"
  bug-reports: "https://github.com/ocamllabs/ocaml-integers/issues"
  depends: [
    "ocaml"
    "ocamlbuild" {build}
    "ocamlfind" {build}
    "topkg" {build}
  ]
  build: ["ocaml" "pkg/pkg.ml" "build" "--pinned" "%{pinned}%"]
  dev-repo: "git+https://github.com/ocamllabs/ocaml-integers.git"
  url {
    src:
     
  "https://github.com/ocamllabs/ocaml-integers/releases/download/v0.2.2/integers-0.2.2.tbz"
    checksum: "md5=ae226532930965fe0b43c02f2469cadc"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  ocamlbuild, findlib, topkg }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in

stdenv.mkDerivation rec {
  pname = "integers";
  version = "0.2.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocamllabs/ocaml-integers/releases/download/v0.2.2/integers-0.2.2.tbz";
    sha256 = "08b1ljw88ny3l0mdq6xmffjk8anfc77igryva5jz1p6f4f746ywk";
  };
  buildInputs = [
    ocaml ocamlbuild findlib topkg ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pinned'" "false" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
