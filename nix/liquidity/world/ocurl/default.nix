/*opam-version: "2.0"
  name: "ocurl"
  version: "0.8.2"
  synopsis: "Bindings to libcurl"
  description: """
  Client-side URL transfer library, supporting HTTP and a multitude of
  other network protocols (FTP/SMTP/RTSP/etc)."""
  maintainer: "ygrek@autistici.org"
  authors: ["Lars Nilsson" "ygrek"]
  license: "MIT"
  tags: ["org:ygrek" "clib:curl"]
  homepage: "http://ygrek.org.ua/p/ocurl/"
  doc: "http://ygrek.org.ua/p/ocurl/api/index.html"
  bug-reports: "https://github.com/ygrek/ocurl/issues"
  depends: [
    "ocaml" {>= "4.02.0"}
    "ocamlfind" {build}
    "base-unix"
    "conf-libcurl"
  ]
  depopts: ["lwt"]
  flags: light-uninstall
  build: [
    ["./configure"]
    [make]
    [make "test"] {with-test}
    [make "doc"] {with-doc}
  ]
  install: [make "install"]
  remove: ["ocamlfind" "remove" "curl"]
  dev-repo: "git://github.com/ygrek/ocurl.git"
  url {
    src: "http://ygrek.org.ua/p/release/ocurl/ocurl-0.8.2.tar.gz"
    checksum: "md5=194b65b5abb1e32be17f4d5c8c9a8627"
    mirrors:
     
  "https://github.com/ygrek/ocurl/releases/download/0.8.2/ocurl-0.8.2.tar.gz"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  findlib, base-unix, conf-libcurl, lwt ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.02.0") >= 0;

stdenv.mkDerivation rec {
  pname = "ocurl";
  version = "0.8.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "http://ygrek.org.ua/p/release/ocurl/ocurl-0.8.2.tar.gz";
    sha256 = "1ax3xdlzgb1zg7d0wr9nwgmh6a45a764m0wk8p6mx07ad94hz0q9";
  };
  buildInputs = [
    ocaml findlib base-unix conf-libcurl ]
  ++
  stdenv.lib.optional
  (lwt
  !=
  null)
  lwt;
  propagatedBuildInputs = [
    ocaml base-unix conf-libcurl ]
  ++
  stdenv.lib.optional
  (lwt
  !=
  null)
  lwt;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'./configure'" ] [ "make" ] (stdenv.lib.optionals doCheck [
      "make" "'test'" ])
    [ "make" "'doc'" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
