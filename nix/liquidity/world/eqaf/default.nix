/*opam-version: "2.0"
  name: "eqaf"
  version: "0.2"
  synopsis: "Constant-time equal function on string"
  description:
    "This package provides an equal function on string in constant-time to
  avoid timing-attack with crypto stuff."
  maintainer: "Romain Calascibetta <romain.calascibetta@gmail.com>"
  authors: "Romain Calascibetta <romain.calascibetta@gmail.com>"
  license: "MIT"
  homepage: "https://github.com/dinosaure/eqaf"
  doc: "https://dinosaure.github.io/eqaf/"
  bug-reports: "https://github.com/dinosaure/eqaf/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {build}
    "fmt" {with-test}
    "base-bytes" {with-test}
  ]
  build: [
    ["dune" "subst"]
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/dinosaure/eqaf.git"
  url {
    src:
     
  "https://github.com/dinosaure/eqaf/releases/download/v0.2/eqaf-v0.2.tbz"
    checksum: "md5=0832ef11a70fe5fe0c786fcf2a2a67cb"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml, dune,
  fmt ? null, base-bytes ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.03.0") >= 0;

stdenv.mkDerivation rec {
  pname = "eqaf";
  version = "0.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/dinosaure/eqaf/releases/download/v0.2/eqaf-v0.2.tbz";
    sha256 = "1la6hdfr91m969c7f6a3nil4895pqsha4w6mcm0rii4h8i0bdkxi";
  };
  buildInputs = [
    ocaml dune ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  stdenv.lib.optional
  doCheck
  base-bytes
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  stdenv.lib.optional
  doCheck
  base-bytes;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'subst'" ] [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ]
    (stdenv.lib.optionals doCheck [
      "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ])
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
